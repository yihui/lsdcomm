## lsdcomm - 不再更新，请使用 [pqcom](https://github.com/xiongyihui/pqcom)
串口工具，是https://code.google.com/p/lsdcomm/的改进

## 特点
在众多的调试工具之中,这款主要是增加如下功能:

+ 文件存档。不同的协议可以做成一个文件存档。
+ 支持脚本指令。
+ 增加其他功能如校验功能(累加和,CRC)。
+ 保存历史的发送指令，省去你重新输入。
+ 自动升级

## 下载
[lsdcomm-latest.exe](http://git.oschina.net/yihui/lsdcomm/raw/master/release/LSDComm-latest.exe)

## 截图
![main](screenshot/main.png)

![command](screenshot/command.png)